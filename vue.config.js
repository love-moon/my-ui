module.exports = {
    // 选项...
    devServer: {
        proxy: {
            '/user': {
                target: 'http://localhost:8099/user/',
                changeOrigin: true,
                pathRewrite: {
                    '^/user': ''//这里理解成用‘/api’代替target里面的地址，后面组件中我们掉接口时直接用api代替 比如我要调用'http://40.00.100.100:3002/user/add'，直接写‘/api/user/add’即可
                }
            },
            '/oauth': {
                target: 'http://localhost:8099/oauth/',
                changeOrigin: true,
                pathRewrite: {
                    '^/oauth': ''
                }
            },
            '/menu': {
                target: 'http://localhost:8099/menu/',
                changeOrigin: true,
                pathRewrite: {
                    '^/menu': ''
                }
            },
            '/file': {
                target: 'http://localhost:8099/file/',
                changeOrigin: true,
                pathRewrite: {
                    '^/file': ''
                }
            }
            ,
            '/upload': {
                target: 'http://localhost:8012/file/upload/',
                changeOrigin: true,
                pathRewrite: {
                    '^/upload': ''
                }
            },
            '/share': {
                target: 'http://localhost:8012/share/',
                changeOrigin: true,
                pathRewrite: {
                    '^/share': ''
                }
            }

        }
    }
}