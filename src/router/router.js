import Vue from 'vue'
import Router from 'vue-router'

import Index from '../pages/index'
import login from '../components/login'
import Upload from '../components/upload'
import register from '../components/register'
import container from '../components/container'
import share from '../components/share'
import myshare from '../components/myshare'
import NotFoundComponent from '../pages/NotFoundComponent'

Vue.use(Router)

export default new Router({
    routes: [

        {
            path: '/',
            name: 'login',
            component: login
        },
        {
            path: '/register',
            name: 'register',
            component: register
        },
        {
            path: '/index',
            name: 'index',
            component: Index,
            children:[
                {
                path: '/upload',
                name: 'upload',
                component: Upload
                },
                {
                    path: '/container',
                    name: 'container',
                    component: container
                },
                {
                    path: '/myshare',
                    name: 'myshare',
                    component: myshare
                },
        ]
        },
        {
            path: '/share/:k',
            name: 'share',
            component: share,
            props: true
        },
        { 
            path: '*', 
            name: '404',
            component: NotFoundComponent
        }

    

    ]
})