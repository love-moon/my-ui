import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VeeValidate from 'vee-validate';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import Vuetify from 'vuetify'
import axios from "axios"
import VueRouter from 'vue-router'
import router from './router/router.js'
import uploader from 'vue-simple-uploader'


Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(uploader)

Vue.config.productionTip = false
Vue.prototype.$http = axios
new Vue({
  render: h => h(App),
  components: { App },
  template: '<App/>',
  router
}).$mount('#app')

router.beforeEach((to, from, next) => {
  var token1 = sessionStorage.getItem('token');
  if (token1!=null&&token1!=""&&token1!="undefine") { //如果有就直接到首页咯
    next();
  } else {
    if (to.path == '/') { //如果是登录页面路径，就直接next()
      next();
    } else { //不然就跳转到登录；
      alert("未登录！");
      next('/');
    }

  }
})

